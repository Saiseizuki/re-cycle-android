package materialexplore.com.materialexplore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import materialexplore.com.materialexplore.MyApplication;

/**
 * Created by xcptan on 9/5/15.
 */
public class Prefs {

    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String ACCESS_TOKEN = "access_token";

    public static final String NAME = "name";
    public static final String IMAGE_URL = "image_url";
    public static final String GREENIES= "greenies";
    public static final String CARBON = "carbon";
    public static final String DISTANCE = "distance";
    public static final String ID = "id";
    public static final String TOKEN = "token";
    public static final String RIDE_CODE = "ride_code";
    public static final String REG_ID = "reg_id";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static Prefs sInstance;

    public Prefs() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication
                .getAppContext());
        editor = sharedPreferences.edit();
    }

    public static Prefs getInstance() {
        if (sInstance == null) {
            sInstance = new Prefs();
        }
        return sInstance;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        editor.putBoolean(IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void setAccessToken(String accessToken) {
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN, "");
    }

    public void setToken(String token) {
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public String getToken() {
        return sharedPreferences.getString(TOKEN, "N/A");
    }

    public void setName(String token) {
        editor.putString(NAME, token);
        editor.commit();
    }

    public String getName() {
        return sharedPreferences.getString(NAME, "N/A");
    }

    public void setImageUrl(String token) {
        editor.putString(IMAGE_URL, token);
        editor.commit();
    }

    public String getImageUrl() {
        return sharedPreferences.getString(IMAGE_URL, "N/A");
    }

    public void setGreenies(String token) {
        editor.putString(GREENIES, token);
        editor.commit();
    }

    public String getGreenies() {
        return sharedPreferences.getString(GREENIES, "N/A");
    }

    public void setCarbon(String token) {
        editor.putString(CARBON, token);
        editor.commit();
    }

    public String getCarbon() {
        return sharedPreferences.getString(CARBON, "N/A");
    }

    public void setDistance(String token) {
        editor.putString(DISTANCE, token);
        editor.commit();
    }

    public String getDistance() {
        return sharedPreferences.getString(DISTANCE, "N/A");
    }

    public void setId(String token) {
        editor.putString(ID, token);
        editor.commit();
    }

    public String getId() {
        return sharedPreferences.getString(ID, "N/A");
    }

    public void setRideCode(String token) {
        editor.putString(RIDE_CODE, token);
        editor.commit();
    }

    public String getRideCode() {
        return sharedPreferences.getString(RIDE_CODE, "N/A");
    }

    public void setRegId(String token) {
        editor.putString(REG_ID, token);
        editor.commit();
    }

    public String getRegId() {
        return sharedPreferences.getString(REG_ID, "N/A");
    }
}
