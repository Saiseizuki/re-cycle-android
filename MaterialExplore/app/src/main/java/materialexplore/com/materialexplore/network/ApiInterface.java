package materialexplore.com.materialexplore.network;

import com.google.gson.JsonElement;

import materialexplore.com.materialexplore.network.model.End;
import materialexplore.com.materialexplore.network.model.Join;
import materialexplore.com.materialexplore.network.model.Share;
import materialexplore.com.materialexplore.network.model.Start;
import materialexplore.com.materialexplore.network.model.SummaryResponse;
import materialexplore.com.materialexplore.network.model.User;
import materialexplore.com.materialexplore.network.model.UserResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by xcptan on 9/5/15.
 */
public interface ApiInterface {

    @POST("/v1/users")
    public void getUserProfile(@Body User user, Callback<UserResponse> callback);

    @GET("/v1/rides/code")
    public void createRoom(@Header("token") String token, Callback<JsonElement> callback);

    @POST("/v1/rides/start")
    public void startRide(@Header("token") String token, @Body Start start, Callback<JsonElement> callback);

    @POST("/v1/rides/join")
    public void joinRoom(@Header("token") String token, @Body Join join, Callback<JsonElement> callback);

    @POST("/v1/rides/stop")
    public void endRide(@Header("token") String token, @Body End end, Callback<SummaryResponse> callback);

    @POST("/v1/rides/share")
    public void share(@Header("token") String token, @Body Share share, Callback<JsonElement> callback);

}
