package materialexplore.com.materialexplore.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.activity.RidingActivity;
import materialexplore.com.materialexplore.utils.Prefs;

/**
 * Created by jadeantolingaa on 9/5/15.
 */
public class HomeFragment extends Fragment {
    OnHomeFragmentListener mOnHomeFragmentListener;

    public interface OnHomeFragmentListener {
        public void onRideWithFriendsClick();
    }

    @Bind(R.id.fragment_home_textview_level)
    TextView mTextViewLevel;

    @Bind(R.id.fragment_home_textview_greenies)
    TextView mTextViewGreenies;

    @Bind(R.id.fragment_home_textview_c02)
    TextView mTextViewCo2;

    @Bind(R.id.fragment_home_imageview_level)
    ImageView mImageViewLevel;


    @OnClick(R.id.fragment_home_button_ride_friends)
    public void onRideFriendsButtonClick(View view) {
        mOnHomeFragmentListener.onRideWithFriendsClick();
    }

    @OnClick(R.id.fragment_home_button_ride_solo)
    public void onRideSoloButtonClick(View view) {
        startActivity(new Intent(getActivity(), RidingActivity.class));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mOnHomeFragmentListener = (OnHomeFragmentListener) getActivity();

        int totalGreen = Integer.parseInt(Prefs.getInstance().getGreenies());
        if (totalGreen == 0) {
            mTextViewLevel.setText("Level 1");
            mImageViewLevel.setBackgroundResource(R.drawable.game_level1_big);
        } else if (totalGreen >= 1 && totalGreen <= 5) {
            mTextViewLevel.setText("Level 2");
            mImageViewLevel.setBackgroundResource(R.drawable.game_lvl2);
        } else if (totalGreen >= 6) {
            mTextViewLevel.setText("Level 3");
            mImageViewLevel.setBackgroundResource(R.drawable.game_lvl2);
        }

        mTextViewGreenies.setText("Greenies x " + Prefs.getInstance().getGreenies());
        mTextViewCo2.setText("Co2 reduced today: " + Prefs.getInstance().getCarbon() + " kg");
    }

}
