package materialexplore.com.materialexplore.network.model;

/**
 * Created by xcptan on 9/6/15.
 */
public class End {

    private String code;

    private String total_distance;

    public End(String code, String total_distance) {
        this.code = code;
        this.total_distance = total_distance;
    }
}
