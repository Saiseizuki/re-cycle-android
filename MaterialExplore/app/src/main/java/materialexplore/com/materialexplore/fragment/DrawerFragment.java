package materialexplore.com.materialexplore.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.OnClick;
import materialexplore.com.materialexplore.R;

/**
 * Created by jadeantolingaa on 8/19/15.
 */
public class DrawerFragment extends Fragment {

    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawerLayout;

    public DrawerFragment() {
    }

    public static DrawerFragment newInstance() {
        DrawerFragment drawerFragment = new DrawerFragment();
        return drawerFragment;
    }

    @OnClick({
            R.id.fragment_drawer_view_group_home,
            R.id.fragment_drawer_view_group_home2,
            R.id.fragment_drawer_view_group_home3,
            R.id.fragment_drawer_view_group_home4,
            R.id.fragment_drawer_image_view_photo})
    public void onDrawerMenuClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_drawer_view_group_home:
                Log.d("Drawer", "home");
                break;
            case R.id.fragment_drawer_view_group_home2:
                Log.d("Drawer", "home2");
                break;
            case R.id.fragment_drawer_view_group_home3:
                Log.d("Drawer", "home3");
                break;
            case R.id.fragment_drawer_view_group_home4:
                Log.d("Drawer", "home4");
                break;

            default:
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer, container, false);
    }

    public void setUpDrawer(DrawerLayout drawerLayout, Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar,
                                            R.string.pma_drawer_open, R.string.pma_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });
    }


}
