package materialexplore.com.materialexplore.network.model;

/**
 * Created by xcptan on 9/6/15.
 */
public class Join {

    private String code;

    private String reg_id;

    public Join(String code, String reg_id) {
        this.code = code;
        this.reg_id = reg_id;
    }
}
