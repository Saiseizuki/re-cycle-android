package materialexplore.com.materialexplore;

import android.app.Application;

import materialexplore.com.materialexplore.network.model.SummaryResponse;

/**
 * Created by xcptan on 9/5/15.
 */
public class MyApplication extends Application {

    public static SummaryResponse sSummaryResponse;

    private static Application sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        this.sContext = this;
    }

    public static Application getAppContext(){
        return sContext;
    }
}
