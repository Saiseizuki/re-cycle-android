
package materialexplore.com.materialexplore.network.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("carbon_reduced")
    @Expose
    private String carbonReduced;
    @SerializedName("distance_travelled")
    @Expose
    private String distanceTravelled;
    @SerializedName("average_speed")
    @Expose
    private String averageSpeed;
    @SerializedName("friends_joined")
    @Expose
    private List<FriendsJoined> friendsJoined = new ArrayList<FriendsJoined>();
    @Expose
    private Greenies greenies;

    /**
     * 
     * @return
     *     The carbonReduced
     */
    public String getCarbonReduced() {
        return carbonReduced;
    }

    /**
     * 
     * @param carbonReduced
     *     The carbon_reduced
     */
    public void setCarbonReduced(String carbonReduced) {
        this.carbonReduced = carbonReduced;
    }

    /**
     * 
     * @return
     *     The distanceTravelled
     */
    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    /**
     * 
     * @param distanceTravelled
     *     The distance_travelled
     */
    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    /**
     * 
     * @return
     *     The averageSpeed
     */
    public String getAverageSpeed() {
        return averageSpeed;
    }

    /**
     * 
     * @param averageSpeed
     *     The average_speed
     */
    public void setAverageSpeed(String averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    /**
     * 
     * @return
     *     The friendsJoined
     */
    public List<FriendsJoined> getFriendsJoined() {
        return friendsJoined;
    }

    /**
     * 
     * @param friendsJoined
     *     The friends_joined
     */
    public void setFriendsJoined(List<FriendsJoined> friendsJoined) {
        this.friendsJoined = friendsJoined;
    }

    /**
     * 
     * @return
     *     The greenies
     */
    public Greenies getGreenies() {
        return greenies;
    }

    /**
     * 
     * @param greenies
     *     The greenies
     */
    public void setGreenies(Greenies greenies) {
        this.greenies = greenies;
    }

}
