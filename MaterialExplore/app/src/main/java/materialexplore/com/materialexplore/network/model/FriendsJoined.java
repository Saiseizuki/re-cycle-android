
package materialexplore.com.materialexplore.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FriendsJoined {

    @Expose
    private String id;
    @SerializedName("pic_url")
    @Expose
    private String picUrl;
    @SerializedName("reg_id")
    @Expose
    private String regId;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 
     * @param picUrl
     *     The pic_url
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 
     * @return
     *     The regId
     */
    public String getRegId() {
        return regId;
    }

    /**
     * 
     * @param regId
     *     The reg_id
     */
    public void setRegId(String regId) {
        this.regId = regId;
    }

}
