package materialexplore.com.materialexplore.activity;

/**
 * Created by jadeantolingaa on 8/20/15.
 */
public interface MainActivityContainer {

    void setDrawerLayout();

    void setHomeButtonVisibility(boolean visible);
}
