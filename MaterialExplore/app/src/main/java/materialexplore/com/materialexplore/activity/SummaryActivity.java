package materialexplore.com.materialexplore.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Base64;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import butterknife.Bind;
import materialexplore.com.materialexplore.MyApplication;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.network.Api;
import materialexplore.com.materialexplore.network.model.FriendsJoined;
import materialexplore.com.materialexplore.network.model.Share;
import materialexplore.com.materialexplore.utils.CircleTransform;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by xcptan on 9/6/15.
 */
public class SummaryActivity extends BaseAbstractActivity {

    @Bind(R.id.share_content)
    ViewGroup shareContent;

    @Bind(R.id.friends_image_container)
    LinearLayout friendsContainer;

    @Bind(R.id.share_button)
    Button shareButton;

    @Bind(R.id.ok_button)
    Button okButton;

    @Bind(R.id.activity_summary_textview_friends_join)
    TextView mTextViewFriendsJoin;

    @Bind(R.id.activity_summary_textview_total_greenies_gained)
    TextView mTextViewTotalGreeniesGained;

    @Bind(R.id.activity_summary_textview_bonus_greenie)
    TextView mTextViewBonusGreenie;

    @Bind(R.id.activity_summary_textview_challenge_greenie)
    TextView mTextViewChallengeGreenie;

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);


        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources()
                .getDisplayMetrics());

        ImageView imageView = new ImageView(this);
        //setting image resource
        Picasso.with(this).load(Prefs.getInstance().getImageUrl()).placeholder(R.drawable
                .profile_photo).transform(CircleTransform.getInstance()).into(imageView);
        //setting image position

        imageView.setLayoutParams(new LinearLayout.LayoutParams(
                size, size));

        friendsContainer.addView(imageView);

        for (FriendsJoined friendsJoined : MyApplication.sSummaryResponse.getData().getFriendsJoined()){
            ImageView toAdd = new ImageView(this);
            //setting image resource
            Picasso.with(this).load(friendsJoined.getPicUrl()).placeholder(R.drawable
                    .profile_photo).transform(CircleTransform.getInstance()).into(toAdd);
            //setting image position

            toAdd.setLayoutParams(new LinearLayout.LayoutParams(
                    size, size));
            friendsContainer.addView(toAdd);
        }

        mTextViewFriendsJoin.setText("Friends joined you: x" + MyApplication.sSummaryResponse.getData().getFriendsJoined().size());
        mTextViewTotalGreeniesGained.setText("Total Greenies gained: " + MyApplication.sSummaryResponse.getData().getGreenies().getTotalGreeniesGained());
        mTextViewBonusGreenie.setText("(Bonus Greenie +" + MyApplication.sSummaryResponse.getData().getGreenies().getBonusGreenie() + ")");
        mTextViewChallengeGreenie.setText("(Challenge Bonus: Greenie +" + MyApplication.sSummaryResponse.getData().getGreenies().getChallengeBonusGreenie() + ")");
        //adding view to layout


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SummaryActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                screenShot(shareContent).compress(Bitmap.CompressFormat.PNG, 100,
                        byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();

                String encodedBase64Image = Base64.encodeToString(byteArray, Base64.DEFAULT);

                Api.getInstance().share(Prefs.getInstance().getAccessToken(), new Share(Prefs
                        .getInstance().getId(), encodedBase64Image, "Bike pa more!"), new
                        Callback<JsonElement>() {

                            @Override
                            public void success(JsonElement jsonElement, Response response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(new Gson().toJson
                                            (jsonElement));
                                    Toast.makeText(SummaryActivity.this, jsonObject.getJSONObject
                                            ("data").getString("message"), Toast.LENGTH_LONG)
                                            .show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });

                //DO API CALL HERE
            }
        });
    }

    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
}
