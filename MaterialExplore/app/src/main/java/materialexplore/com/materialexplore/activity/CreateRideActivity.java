package materialexplore.com.materialexplore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.network.Api;
import materialexplore.com.materialexplore.network.model.Start;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by xcptan on 9/6/15.
 */
public class CreateRideActivity extends BaseAbstractActivity {
    @Bind(R.id.start_button)
    Button startButton;

    @Bind(R.id.textview_code)
    TextView textViewCode;

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ride);

        Api.getInstance().createRoom(Prefs.getInstance().getToken(), new Callback<JsonElement>() {

            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson
                            (jsonElement));
                    Prefs.getInstance().setRideCode(jsonObject.getJSONObject("data").getString
                            ("code"));
                    textViewCode.setText(jsonObject.getJSONObject("data").getString("code"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api.getInstance().startRide(Prefs.getInstance().getToken(), new Start(Prefs
                        .getInstance().getRideCode()), new Callback<JsonElement>() {

                    @Override
                    public void success(JsonElement jsonElement, Response response) {
                            startActivity(new Intent(CreateRideActivity.this,RidingActivity.class));

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        });
    }
}
