package materialexplore.com.materialexplore.network.model;

/**
 * Created by xcptan on 9/6/15.
 */
public class Share {

    private String id;

    private String picture;

    private String message;

    public Share(String id, String picture, String message) {
        this.id = id;
        this.picture = picture;
        this.message = message;
    }
}
