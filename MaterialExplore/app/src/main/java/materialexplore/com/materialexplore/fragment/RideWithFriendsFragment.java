package materialexplore.com.materialexplore.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.activity.CreateRideActivity;
import materialexplore.com.materialexplore.activity.RidingActivity;
import materialexplore.com.materialexplore.activity.StoreDetailActivity;
import materialexplore.com.materialexplore.adapter.ImageAdapter;
import materialexplore.com.materialexplore.network.Api;
import materialexplore.com.materialexplore.network.model.Join;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jadeantolingaa on 9/6/15.
 */
public class RideWithFriendsFragment extends Fragment {

    @Bind(R.id.radio_group)
    RadioGroup radioGroup;

    @Bind(R.id.fragment_home_button_ride_friends)
    Button okayButton;

    @Bind(R.id.fragment_ride_with_friends_edittext_code)
    EditText editText;

    int selected = R.id.radio_create;

    Activity mActivity;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);

            startActivity(new Intent(getActivity(), RidingActivity.class));
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ride_with_friends, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
        LocalBroadcastManager.getInstance(activity).registerReceiver(mMessageReceiver,
                new IntentFilter("auto-start"));
    }

    @Override
    public void onDetach() {
        super.onDetach();

        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selected = checkedId;
            }
        });

        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("selected", selected + "");
                if (selected == R.id.radio_create) {
                    startActivity(new Intent(getActivity(), CreateRideActivity.class));
                } else if (selected == R.id.radio_join) {
                    final String code = editText.getText().toString();


                    Api.getInstance().joinRoom(Prefs.getInstance().getToken(), new Join(code,
                            Prefs.getInstance().getRegId()), new Callback<JsonElement>() {

                        @Override
                        public void success(JsonElement jsonElement, Response response) {
                            try {
                                Prefs.getInstance().setRideCode(code);
                                JSONObject jsonObject = new JSONObject(new Gson().toJson
                                        (jsonElement));
                                Toast.makeText(getActivity(), jsonObject.getJSONObject("data")
                                        .getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Ride With Friends");
    }
}
