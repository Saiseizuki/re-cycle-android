package materialexplore.com.materialexplore.network.model;

/**
 * Created by xcptan on 9/5/15.
 */
public class UserResponse {

    String status;
    Data data;

    public String getStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public static class Data{
        String name;
        String image_url;
        String greenies;
        String carbon;
        String distance;
        String id;
        String token;

        public String getName() {
            return name;
        }

        public String getImageUrl() {
            return image_url;
        }

        public String getGreenies() {
            return greenies;
        }

        public String getCarbon() {
            return carbon;
        }

        public String getDistance() {
            return distance;
        }

        public String getId() {
            return id;
        }

        public String getToken() {
            return token;
        }
    }
}
