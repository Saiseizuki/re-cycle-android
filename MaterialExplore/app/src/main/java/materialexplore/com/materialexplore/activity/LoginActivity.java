package materialexplore.com.materialexplore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.DefaultAudience;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Logger;

import java.util.List;

import butterknife.Bind;
import materialexplore.com.materialexplore.MyApplication;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.network.Api;
import materialexplore.com.materialexplore.network.model.User;
import materialexplore.com.materialexplore.network.model.UserResponse;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by xcptan on 9/5/15.
 */
public class LoginActivity extends BaseAbstractActivity {

    Permission[] permissions = new Permission[]{
            Permission.PUBLIC_PROFILE,
            Permission.EMAIL,
            Permission.USER_EVENTS,
            Permission.USER_ACTIONS_MUSIC,
            Permission.USER_FRIENDS,
            Permission.USER_GAMES_ACTIVITY,
            Permission.USER_BIRTHDAY,
            Permission.USER_GROUPS,
            Permission.PUBLISH_ACTION};

    private SimpleFacebook mSimpleFacebook;

    private final String TAG = LoginActivity.class.getSimpleName();

    final OnProfileListener onProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            Log.i(TAG, "My profile id = " + profile.getId());

            Log.i(TAG, "My name= " + profile.getName());

            Log.i(TAG, "My picture= " + "http://graph.facebook.com/" + profile.getId() +
                    "/picture?type=normal");

            Api.getInstance().getUserProfile(new User(profile.getName(), profile.getId(),
                    "http://graph.facebook.com/" + profile.getId() + "/picture?type=normal",
                    Prefs.getInstance().getAccessToken()), new
                    Callback<UserResponse>() {

                        @Override
                        public void success(UserResponse userResponse, Response response) {
                            Prefs instance = Prefs.getInstance();
                            UserResponse.Data data = userResponse.getData();
                            instance.setIsLoggedIn(true);
                            instance.setName(data.getName());
                            instance.setImageUrl(data.getImageUrl());
                            instance.setGreenies(data.getGreenies());
                            instance.setCarbon(data.getCarbon());
                            instance.setDistance(data.getDistance());
                            instance.setId(data.getId());
                            instance.setToken(data.getToken());

                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(LoginActivity.this, "Failed to logged in: " + error
                                    .getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

        }
    };

    final OnLoginListener onLoginListener = new OnLoginListener() {

        @Override
        public void onFail(String reason) {
            Log.w(TAG, "Failed to login");
        }

        @Override
        public void onException(Throwable throwable) {
            Log.e(TAG, "Bad thing happened", throwable);
        }

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions,
                            List<Permission> declinedPermissions) {
            Log.e(TAG, "Login success! " + accessToken);

            Prefs.getInstance().setAccessToken(accessToken);

            mSimpleFacebook.getProfile(onProfileListener);
        }




        @Override
        public void onCancel() {
            Log.w(TAG, "Canceled the login");
        }

    };

    @Bind(R.id.login_button)
    Button loginButton;

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Logger.DEBUG_WITH_STACKTRACE = true;

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.fb_app_id))
                .setNamespace(getString(R.string.app_name))
                .setPermissions(permissions)
                .setDefaultAudience(DefaultAudience.FRIENDS)
                .setAskForAllPermissionsAtOnce(true)
                .build();

        SimpleFacebook.setConfiguration(configuration);

        mSimpleFacebook = SimpleFacebook.getInstance(this);

        if (Prefs.getInstance().isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSimpleFacebook.login(onLoginListener);
                ;
//                final Dialog dialog = new Dialog(LoginActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
//                dialog.setContentView(R.layout.dialog_webview);
//                dialog.setTitle("Login with Facebook");
//                dialog.setCancelable(true);
//
//                dialog.show();
//
//                WebView wv = (WebView) dialog.findViewById(R.id.webView);
//                wv.getSettings().setJavaScriptEnabled(true);
//                wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//                wv.setWebViewClient(new WebViewClient() {
//                    @Override
//                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                        view.loadUrl(url);
//                        Log.d(TAG, "WebView url: " + url);
//
//                        if (url.endsWith("/redirect")) {
//                            dialog.dismiss();
//                            Log.e(TAG, "Login success from webview!");
//
//                            Prefs.getInstance().setIsLoggedIn(true);
//
//                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                            finish();
//                        }
//
//                        return true;
//                    }
//                });
//                wv.loadUrl("http://52.76.93.75:1330");
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
