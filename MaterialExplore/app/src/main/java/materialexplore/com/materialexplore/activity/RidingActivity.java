package materialexplore.com.materialexplore.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import materialexplore.com.materialexplore.MyApplication;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.network.Api;
import materialexplore.com.materialexplore.network.model.End;
import materialexplore.com.materialexplore.network.model.SummaryResponse;
import materialexplore.com.materialexplore.utils.DirectionsJSONParser;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by xcptan on 9/5/15.
 */
public class RidingActivity extends BaseAbstractActivity implements OnMapReadyCallback {
    private static final String TAG = RidingActivity.class.getSimpleName();

    int mMode = 0;

    @Bind(R.id.activity_riding_button_stop)
    Button stopButton;

    @Bind(R.id.teleport_button)
    Button mTeleportButton;

    private GoogleMap mMap;

    LatLng currLatLng;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);

            Api.getInstance().endRide(Prefs.getInstance().getToken(), new End(Prefs
                    .getInstance().getRideCode(), "45"), new Callback<SummaryResponse>() {
                @Override
                public void success(SummaryResponse summaryResponse, Response response) {
                    MyApplication.sSummaryResponse = summaryResponse;
                    startActivity(new Intent(RidingActivity.this, SummaryActivity.class));
                    finish();
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    };

    @Override
    public Activity getActivity() {
        return this;
    }


    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riding);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("auto-stop"));

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api.getInstance().endRide(Prefs.getInstance().getToken(), new End(Prefs
                        .getInstance().getRideCode(), "45"), new Callback<SummaryResponse>() {
                    @Override
                    public void success(SummaryResponse summaryResponse, Response response) {
                        MyApplication.sSummaryResponse = summaryResponse;
                        startActivity(new Intent(RidingActivity.this,SummaryActivity.class));
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        });
            mTeleportButton.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View v){
                Log.d(TAG, "Tele clicked");
//                mMap.clear();
//
//                LatLng fence = new LatLng(14.5557372,121.0235196);
//
//                mMap.addMarker( new MarkerOptions()
//                        .position(fence)
//                        .title("La Mesa Dam")
//                        .snippet("Eco-Zone")).showInfoWindow();
//
////Instantiates a new CircleOptions object +  center/radius
//                CircleOptions circleOptions = new CircleOptions()
//                        .center( fence)
//                        .radius(75.0f)
//                        .fillColor(0x2F00ff00)
//                        .strokeColor(Color.GREEN)
//                        .strokeWidth(4);
//
//// Get back the mutable Circle
//                Circle circle = mMap.addCircle(circleOptions);
//
//                mMap.addMarker(new MarkerOptions().position(new LatLng(14.553167, 121.025722))
// .title("Marker in Makati"));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.553167, 121
// .025722), 17.0f));
            }
            }

            );
        }

        @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }

        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Log.d(TAG, "Map ready");

        currLatLng = new LatLng(14.553167, 121.025722);

        LatLng centerLatLng = new LatLng(14.555125, 121.024839);

        LatLng fence = new LatLng(14.556683, 121.024536);

        MarkerOptions marker = new MarkerOptions().position(currLatLng).title("Marker in Makati");
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_level1));

        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerLatLng, 17.0f));

        mMap.addMarker(new MarkerOptions()
                .position(fence)
                .title("La Mesa Dam")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_level2))
                .snippet("Eco-Zone")).showInfoWindow();

//Instantiates a new CircleOptions object +  center/radius
        CircleOptions circleOptions = new CircleOptions()
                .center(fence)
                .radius(170.0f)
                .fillColor(0x2F00ff00)
                .strokeColor(Color.GREEN)
                .strokeWidth(4);

// Get back the mutable Circle
        Circle circle = mMap.addCircle(circleOptions);

        ArrayList<LatLng> markerPoints = new ArrayList<LatLng>();

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(currLatLng, fence);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

//        PolylineOptions line=
//                new PolylineOptions().add(new LatLng(14.553167, 121.025722),
//                        new LatLng(14.5557372,121.0235196))
//                        .width(20).color(Color.BLUE);

//        mMap.addPolyline(line);

    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,
            String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);

                // Changing the color polyline according to the mode
//                if(mMode==MODE_DRIVING)
//                    lineOptions.color(Color.RED);
//                else if(mMode==MODE_BICYCLING)
//                    lineOptions.color(Color.GREEN);
//                else if(mMode==MODE_WALKING)
                lineOptions.color(Color.BLUE);
            }

            if (result.size() < 1) {
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Travelling Mode
        String mode = "mode=walking";
        mMode = 2;


        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }
}
