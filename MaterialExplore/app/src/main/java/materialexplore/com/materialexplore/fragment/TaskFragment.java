package materialexplore.com.materialexplore.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import materialexplore.com.materialexplore.R;

/**
 * Created by jadeantolingaa on 9/6/15.
 */
public class TaskFragment extends Fragment {

    public static TaskFragment getInstance() {
        TaskFragment taskFragment = new TaskFragment();
        return taskFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task, container, false);
    }
}
