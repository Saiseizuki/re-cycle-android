package materialexplore.com.materialexplore.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import materialexplore.com.materialexplore.R;


public class GcmReceiver extends BroadcastReceiver {
    public static final int LBM_NOTIFICATION_ID = 526;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Gcm","onReceive " + intent.getAction());
        Log.i("Gcm", "data: " + intent.getExtras().toString());


        if (intent.getAction().equals(
                "com.google.android.c2dm.intent.RECEIVE")) {
            Log.i("Gcm", "receive: " + intent.getData());
            Log.d("@@@Intent", intent.getExtras().getString("message"));

            Intent startIntent= new Intent("auto-start");
            LocalBroadcastManager.getInstance(context).sendBroadcast(startIntent);

            Intent stopIntent= new Intent("auto-stop");
            LocalBroadcastManager.getInstance(context).sendBroadcast(stopIntent);

//            JSONObject data = null;

//            try {
////                data = new JSONObject(intent.getExtras().getString("data"));
////                String message = data.getString("message");
//
//
//                if (message.contains("started")) {
////                    builder.setContentTitle("Get ready! Starting to Ride...");
////                    builder.setContentText("Now let's go! Kick some ass baby!");
//                } else {
//                    builder.setContentTitle("Finally! You've got arrived...");
//                    builder.setContentText("Stronger with Re-cycle!");
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.mipmap.ic_launcher);

            String v = intent.getExtras().getString("message");

            if (v.contains("started")) {
                    builder.setContentTitle("Get ready! Starting to Ride...");
                    builder.setContentText("Now let's go! Kick some ass baby!");
                } else {
                    builder.setContentTitle("Finally! You've got arrived...");
                    builder.setContentText("Stronger with Re-cycle!");
                }
            NotificationManager notifManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            notifManager.notify(getID(), builder.build());
        }
        setResultCode(Activity.RESULT_OK);
    }

    private final static AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }
}
