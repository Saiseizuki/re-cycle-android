package materialexplore.com.materialexplore.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import materialexplore.com.materialexplore.MyApplication;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.utils.Prefs;

/**
 * Created by jadeantolingaa on 9/5/15.
 */
public class StoreDetailActivity extends BaseAbstractActivity {

    public static final String EXTRA_NAME = "cheese_name";

    @Bind(R.id.radio_group)
    RadioGroup radioGroup;

    @Bind(R.id.activity_store_detail_textview_discounted)
    TextView mTextViewDiscounted;

    @Bind(R.id.activity_store_detail_textview_original)
    TextView mTextViewOriginal;

    @Bind(R.id.activity_store_detail_textview_greenies)
    TextView mTextViewGreenies;

    private int total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail);

        Intent intent = getIntent();
        final String cheeseName = intent.getStringExtra(EXTRA_NAME);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Eco-Shirt");


        loadBackdrop();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_none:
                        total = Integer.parseInt(Prefs.getInstance().getGreenies());
                        mTextViewDiscounted.setText("PHP 500.00");
                        mTextViewGreenies.setText("Greenies x " + total);
                        break;
                    case R.id.radio_ten:
                        total = Integer.parseInt(Prefs.getInstance().getGreenies()) - 3;
                        mTextViewDiscounted.setText("PHP 450.00");
                        mTextViewGreenies.setText("Greenies x " + total);
                        break;
                    case R.id.radio_twenty:
                        total = Integer.parseInt(Prefs.getInstance().getGreenies()) - 5;
                        mTextViewDiscounted.setText("PHP 400.00");
                        mTextViewGreenies.setText("Greenies x " + total);
                        break;
                    case R.id.radio_thirty:
                        mTextViewDiscounted.setText("PHP 350.00");
                        total = Integer.parseInt(Prefs.getInstance().getGreenies()) - 8;
                        mTextViewGreenies.setText("Greenies x " + total);
                        break;

                }
            }
        });

        mTextViewGreenies.setText("Greenies x " + Prefs.getInstance().getGreenies());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mTextViewOriginal.setText("PHP 500.00");
        strikeThrough(mTextViewOriginal);
    }

    public void strikeThrough(TextView view) {
        view.setPaintFlags(view.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    private void loadBackdrop() {
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        Glide.with(this).load(R.drawable.tshirt2).centerCrop().into(imageView);
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
