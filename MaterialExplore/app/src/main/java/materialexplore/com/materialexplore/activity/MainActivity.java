package materialexplore.com.materialexplore.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLogoutListener;

import org.w3c.dom.Text;

import java.io.IOException;

import butterknife.Bind;
import materialexplore.com.materialexplore.fragment.ChallengesFragment;
import materialexplore.com.materialexplore.MyApplication;
import materialexplore.com.materialexplore.fragment.CheeseListFragment;
import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.adapter.Adapter;
import materialexplore.com.materialexplore.application.BaseAbstractActivity;
import materialexplore.com.materialexplore.fragment.DrawerFragment;
import materialexplore.com.materialexplore.fragment.HomeFragment;
import materialexplore.com.materialexplore.fragment.RideWithFriendsFragment;
import materialexplore.com.materialexplore.fragment.StoreFragment;
import materialexplore.com.materialexplore.utils.CircleTransform;
import materialexplore.com.materialexplore.utils.Prefs;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseAbstractActivity implements MainActivityContainer,
        DrawerLayout.DrawerListener, HomeFragment.OnHomeFragmentListener {
    private static final int ID_FRAGMENT_CONTAINER = R.id.fragment_container;

    @Bind(R.id.activity_main_drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.fragment_drawer_image_view_photo)
    ImageView mUserPhoto;

    @Bind(R.id.fragment_drawer_text_view_name)
    TextView mUserName;

    @Bind(R.id.fragment_drawer_text_view_co2)
    TextView mTextViewCo2;

    @Bind(R.id.fragment_drawer_text_view_distance)
    TextView mTextViewDistance;

    @Bind(R.id.fragment_drawer_text_view_greenies)
    TextView mTextViewGreenies;

    private DrawerFragment mDrawerFragment;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.mipmap.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        replaceFragment(getActivity(), ID_FRAGMENT_CONTAINER, new HomeFragment());

        setDrawerLayout();

        //setViewPager();
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        Picasso.with(this).load(Prefs.getInstance().getImageUrl()).transform(CircleTransform
                .getInstance()).placeholder(R.drawable.profile_photo).into(mUserPhoto);

        mUserName.setText(Prefs.getInstance().getName());

        mTextViewCo2.setText("Co2 re/cycled: " + Prefs.getInstance().getCarbon() + "kg");
        mTextViewDistance.setText("Distance re/cycled: " + Prefs.getInstance().getDistance()+"km");
        mTextViewGreenies.setText("Greenies gained: "+ Prefs.getInstance().getGreenies());

        registerBackground();
    }

    @Override
    public AppCompatActivity getActivity() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }

//        else if (id == R.id.navigate) {
//            startActivity(new Intent(this, SubActivity.class));
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setDrawerLayout() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.activity_main_drawer);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
//        mDrawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id
// .fragment_drawer);
//        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string
// .pma_drawer_open, R.string.pma_drawer_close);
//        mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
//        mDrawerLayout.setDrawerListener(this);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        int id = menuItem.getItemId();
                        switch (id) {
                            case R.id.nav_home:
                                replaceFragment(getActivity(), ID_FRAGMENT_CONTAINER, new HomeFragment());
                                break;
                            case R.id.nav_challenges:
                                replaceFragment(getActivity(), ID_FRAGMENT_CONTAINER, new ChallengesFragment());
                                break;
                            case R.id.nav_store:
                                replaceFragment(getActivity(), ID_FRAGMENT_CONTAINER, new
                                        StoreFragment());
                                break;
                            case R.id.nav_logout:
                                SimpleFacebook.getInstance().logout(new OnLogoutListener() {
                                    @Override
                                    public void onLogout() {

                                        Prefs.getInstance().setIsLoggedIn(false);

                                        startActivity(new Intent(MainActivity.this, LoginActivity
                                                .class));
                                        finish();
                                    }
                                });
                                break;
                            default:
                                break;
                        }
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    public void setActionBarTitle(int resId) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(resId);
        }
    }

//    private void setViewPager() {
//        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
//        if (viewPager != null) {
//            setupViewPager(viewPager);
//        }
//    }

    @Override
    public void setHomeButtonVisibility(boolean visible) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(visible);
        }
        if (!visible) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    public static void registerBackground() {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(MyApplication
                        .getAppContext());
                String senderId = "903489015119";
                String regid = null;
                try {
                    regid = gcm.register(senderId);
                    Log.d("Gcm", "Regid is "+ regid);
                    Prefs.getInstance().setRegId(regid);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return regid;
            }

            @Override
            protected void onPostExecute(String regId) {
                if (regId != null) {
//                    GcmRequestBody gcmRequestBody = new GcmRequestBody(SecurePreferencesHelper.getInstance
//                            ().getMsisdn(), regId, SecurePreferencesHelper.getInstance()
//                            .getAppName().toLowerCase(), "true");
//
//                    getGcmRestAdapter().register(gcmRequestBody, new Callback<GcmResponse>() {
//                        @Override
//                        public void success(GcmResponse gcmResponse, Response response) {
//                            if (gcmResponse.getStatus() == 200) {
//                                SecurePreferencesHelper.getInstance().setGcmSubmitted(true);
//                            } else {
//                                SecurePreferencesHelper.getInstance().setGcmSubmitted(false);
//                            }
//                        }
//
//                        @Override
//                        public void failure(RetrofitError error) {
//                            SecurePreferencesHelper.getInstance().setGcmSubmitted(false);
//                        }
//                    });
                }
            }
        }.execute();

    }

    @Override
    public void onRideWithFriendsClick() {
        replaceFragment(getActivity(), ID_FRAGMENT_CONTAINER, new RideWithFriendsFragment());
    }
}
