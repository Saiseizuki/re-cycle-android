
package materialexplore.com.materialexplore.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Greenies {

    @Expose
    private int greenie;
    @SerializedName("bonus_greenie")
    @Expose
    private int bonusGreenie;
    @SerializedName("challenge_bonus_greenie")
    @Expose
    private int challengeBonusGreenie;
    @SerializedName("total_greenies_gained")
    @Expose
    private int totalGreeniesGained;

    /**
     * 
     * @return
     *     The greenie
     */
    public int getGreenie() {
        return greenie;
    }

    /**
     * 
     * @param greenie
     *     The greenie
     */
    public void setGreenie(int greenie) {
        this.greenie = greenie;
    }

    /**
     * 
     * @return
     *     The bonusGreenie
     */
    public int getBonusGreenie() {
        return bonusGreenie;
    }

    /**
     * 
     * @param bonusGreenie
     *     The bonus_greenie
     */
    public void setBonusGreenie(int bonusGreenie) {
        this.bonusGreenie = bonusGreenie;
    }

    /**
     * 
     * @return
     *     The challengeBonusGreenie
     */
    public int getChallengeBonusGreenie() {
        return challengeBonusGreenie;
    }

    /**
     * 
     * @param challengeBonusGreenie
     *     The challenge_bonus_greenie
     */
    public void setChallengeBonusGreenie(int challengeBonusGreenie) {
        this.challengeBonusGreenie = challengeBonusGreenie;
    }

    /**
     * 
     * @return
     *     The totalGreeniesGained
     */
    public int getTotalGreeniesGained() {
        return totalGreeniesGained;
    }

    /**
     * 
     * @param totalGreeniesGained
     *     The total_greenies_gained
     */
    public void setTotalGreeniesGained(int totalGreeniesGained) {
        this.totalGreeniesGained = totalGreeniesGained;
    }

}
