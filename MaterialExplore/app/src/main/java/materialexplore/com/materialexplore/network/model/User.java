package materialexplore.com.materialexplore.network.model;

/**
 * Created by xcptan on 9/5/15.
 */
public class User {

    private String name;
    private String id;
    private String image_url;
    private String fb_access_token;

    public User(String name, String id, String image_url, String fb_access_token) {
        this.name = name;
        this.id = id;
        this.image_url = image_url;
        this.fb_access_token = fb_access_token;
    }
}
