package materialexplore.com.materialexplore.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import materialexplore.com.materialexplore.R;
import materialexplore.com.materialexplore.adapter.Adapter;

/**
 * Created by jadeantolingaa on 9/5/15.
 */
public class ChallengesFragment extends Fragment {

    TaskFragment mTaskFragment;
    EcozonesFragment mEcozonesFragment;

    public static ChallengesFragment getInstance() {
        ChallengesFragment challengesFragment = new ChallengesFragment();
        return challengesFragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_challenges, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTaskFragment = new TaskFragment();
        mEcozonesFragment = new EcozonesFragment();
        setViewPager();
    }

    @Override
    public void onStart() {
        super.onStart();
        setViewPager();
    }

    private void setViewPager() {
        ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        Log.d("MainActivity", "@setUpViewPager");
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new TaskFragment(), "Task");
        adapter.addFragment(new EcozonesFragment(), "Eco-zones");
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Challenges");
    }
}
