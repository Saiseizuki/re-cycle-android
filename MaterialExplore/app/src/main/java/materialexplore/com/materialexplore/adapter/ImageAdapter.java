package materialexplore.com.materialexplore.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import materialexplore.com.materialexplore.R;

/**
 * Created by jadeantolingaa on 9/5/15.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(550, 500));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
            R.drawable.tshirt, R.drawable.tshirt2,
    };
}
