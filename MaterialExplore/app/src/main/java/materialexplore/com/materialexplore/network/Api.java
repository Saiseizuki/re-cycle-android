package materialexplore.com.materialexplore.network;

import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by xcptan on 9/5/15.
 */
public class Api {

    private static ApiInterface apiInterface;

    public static ApiInterface getInstance() {
        if (apiInterface == null) {
            apiInterface = new RestAdapter.Builder()
                    .setEndpoint("http://52.76.93.75:1330")
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setConverter(new GsonConverter(new GsonBuilder()
                            .disableHtmlEscaping()
                            .create()))
                    .build
                            ().create(ApiInterface.class);
        }

        return apiInterface;
    }


}
