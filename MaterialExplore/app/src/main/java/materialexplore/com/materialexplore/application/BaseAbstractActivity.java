package materialexplore.com.materialexplore.application;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import materialexplore.com.materialexplore.R;

/**
 * Created by jadeantolingaa on 8/12/15.
 */
public abstract class BaseAbstractActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public abstract Activity getActivity();

    public void setButterKnife() {
        ButterKnife.bind(getActivity());
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
//        initToolbar();
//        initActionBar();
        setButterKnife();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
//        initToolbar();
//        initActionBar();
        setButterKnife();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
//        initToolbar();
//        initActionBar();
        setButterKnife();
    }

    public void initToolbar() {
        View view = findViewById(R.id.app_toolbar);
        if (view != null) {
            setSupportActionBar((Toolbar) view);
            mToolbar = (Toolbar) view;
        }
    }

    public void initActionBar() {
        final ActionBar ab = getSupportActionBar();
        //if (ab != null) {
            ab.setHomeAsUpIndicator(R.mipmap.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        //}
    }

    public static  void replaceFragment(AppCompatActivity activity, int resId, Fragment fragment) {
        activity.getSupportFragmentManager().beginTransaction()
                .replace(resId, fragment, createTag(activity, fragment))
                .commit();
    }

    public static String createTag(AppCompatActivity activity, Fragment fragment) {
        return activity.getClass().getName() + ":" + fragment.getClass().getName();
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}
